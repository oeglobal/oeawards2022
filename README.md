# OEG Awards - Review App

This app is used in review process of OEG Awards.


## Review Process mini-HOWTO

Short manual regarding election process:

0. OEG staff + OEG community: https://awards.oeglobal.org/

     * collect OEG Awards nomination via Gravity Forms
     * assumption: if the form matches previous forms,
       no code changes needed for subsequent steps

1. OEG staff: https://awards.oeglobal.org/

     * obtain URL to Gravity Forms used for latest collection of data
       and put the value into `GFORMS_URL` in `localsettings.py`
       1. login into admin
       2. go into forms
       3. hover over form title to see the URL
       4. note the value in 'id', e.g. for URL like '...&id=1',
          the value is '1'

2. OEG staff: `.../awards/awardsapp/localsettings.py`

     * put the form URL into `GFORMS_URL` in `localsettings.py`,
       e.g. if the ID value is 1, then URL is:
       https://awards.oeglobal.org/wp-json/gf/v2/forms/1/entries

3. OEG staff: `manage.py import_entries`

     * at first, will delete all existing reviews
     * then it will import entries using URL from `GFORMS_URL`

4. OEG staff: `manage.py import_reviewers <XLS file name>`

     * this creates accounts for reviewers listed in the given XLS files
     * XLS file have to contain columns in following order:
       1. first name
       2. last name
       3. email
       4. year -- use current year to ensure the reviewer account is active
       5. is_board_member (yes/no)

5. OEG staff: ensure the descriptions on the rating form are up-to-date

     * run `manage.py print_categories` to print categories
     * open `rating-form.html` and check if all printed categories are present and their descriptions are up-to-date

6. (OPTIONAL) OEG staff: `manage.py init_ratings --reviews <X>`

     * (FYI: in 2024 all reviews were assigned manually, so this step wasn't needed)
     * `X` defines the amount of reviews each entry needs to get
     * `X` is typically `3`
     * this will be a "dry run" meant to review what will be actually done later,
       giving opportunity to tweak `X`, adjust list of viewers, etc.

7. (OPTIONAL) OEG staff: `manage.py init_ratings --reviews <X> --commit`

     * this will actually initialized review process, e.g. content of the database is changed


## Installation / deployment

See [README in ansible subdirectory](ansible/README.md).


## Development

Branches, pull-requests, releases, etc.: according to [git-flow](http://danielkummer.github.io/git-flow-cheatsheet/) but ...

... in order to allow better overview for current and future contributors, we'd like to conclude features with pull-requests,
i.e. instead of `git flow feature finish MYFEATURE` do following:

1. `git flow feature publish MYFEATURE`
2. go to GitLab/GitHub and open pull-request from your feature branch to `develop`
3. review + adjustments
4. merge + delete branch after merge


### Git hooks

For source code formatting, etc.:

`pre-commit install`
