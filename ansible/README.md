# Awards Review - deployment guide

main stuff: see (#Upgrade)

## Initial steps

All assuming roughly that we're running Linux (Ubuntu, ...) on all machines involved.


### On machine running deployment

Let's call it `machine-a`.

1. have SSH key generated, i.e. `~/.ssh/id_rsa` and `~/.ssh/id_rsa.pub`

2. have `ansible` installed

3. have `eoawards` repo checked out

4. have proper content in `<eoawards git repo root>/ansible/inventory/dev`
   and `<eoawards git repo root>/ansible/inventory/prod` (not part of git repo,
   for security reasons; see `example` or contact colleagues)


### On machine we're deploying to

Let's call it `machine-b`.

1. have SSH machine-a's public SSH key (`~/.ssh/id_rsa.pub`) present in the account under which we're deploying:

   `cat <machine-1 id_rsa.pub> >> .ssh/authorized_keys`

   a. hence you are able to SSH from machine-a to <deploy user>@machine-b without supplying password

2. have SSH key generated, i.e. `~/.ssh/id_rsa` and `~/.ssh/id_rsa.pub`

3. have that public SSH key (`~/.ssh/id_rsa.pub`) added as "Deploy key" on GitLab for the project repo

4. have there a `deployment` deploy branch with local changes need
   (TODO: get rid of that branch, i.e. make all necessary stuff configurable via `localsettings.py`)
   
   * exact steps, changes, etc. are out of scope here = "magic" (for now) => TODO later: automate also
     initial deployment

5. have the ability to run `supervisorctl restart awards` without the need to supply password


## Main stuff

### Backup

TODO


### Upgrade

Example for DEV:

`ansible-playbook -i inventory/dev deploy.yml`


### Rollback

TODO
