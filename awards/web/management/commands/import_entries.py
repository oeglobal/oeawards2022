import requests
from django.conf import settings
from django.core.management import BaseCommand

from web.utils_gform import GForm
from web.models import Entry, Category


class Command(BaseCommand):
    help = "imports OE Awards entries from Gravity Forms"

    def handle(self, *args, **options):
        self._import_awards()

    @staticmethod
    def _import_awards():
        response = requests.get(
            settings.GFORMS_URL + "?paging[page_size]=300&_labels=1",
            auth=(settings.GFORM_KEY, settings.GFORM_SECRET),
        )

        # safety checks
        if response.status_code != 200:
            raise ImportError(
                "unexpected status code in response: %d, body: %s"
                % (response.status_code, response.content)
            )
        response_json = response.json()
        if "_labels" not in response_json:
            raise ImportError("labels not found")

        labels = response_json["_labels"]

        entries = []
        for raw_entry in response_json["entries"]:
            entry = {}

            for key, value in raw_entry.items():
                if not value:
                    continue

                try:
                    float(key)
                    if "." in key:
                        j, k = key.split(".")
                        label = labels[j][key]
                        if j == GForm.INDEX_NOMINATOR_NAME:
                            label = "N_" + label
                        if (
                            j == GForm.INDEX_NOMINEE_LEAD_CONTACT
                            or j == GForm.INDEX_NOMINEE_NAME
                        ):
                            label = "C_" + label
                    else:
                        label = labels[key]

                        if key in [
                            GForm.INDEX_NOMINATOR_EMAIL,
                            GForm.INDEX_NOMINATOR_INSTITUTION,
                        ]:
                            label = "N_" + label

                        if key in [
                            GForm.INDEX_NOMINEE_EMAIL,
                            GForm.INDEX_NOMINEE_INSTITUTION,
                        ]:
                            label = "C_" + label

                    label = label.strip(":")
                    entry[label] = value

                    if key in GForm.INDEX_SUBCATEGORIES:
                        entry[GForm.KEY_SUBCATEGORY] = value
                except ValueError:
                    if key == "id":
                        entry["entry_id"] = value

            entries.append(entry)

        Entry.objects.all().delete()
        for data in entries:
            category, is_created = Category.objects.get_or_create(
                name=data.get(GForm.KEY_MAIN_CATEGORY)
            )

            title = data.get(GForm.KEY_TITLE) or "{} {}".format(
                data.get(GForm.KEY_NOMINEE_FIRST_ADJUSTED),
                data.get(GForm.KEY_NOMINEE_LAST_ADJUSTED),
            )

            Entry.objects.create(
                id=data["entry_id"],
                title=title,
                entry_id=data["entry_id"],
                data=data,
                subcategory=data.get(GForm.KEY_SUBCATEGORY, ""),
                category=category,
            )
