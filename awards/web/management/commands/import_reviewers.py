import xlrd

from django.core.management import BaseCommand
from django.contrib.auth.models import User

from web.models import Reviewer


class Command(BaseCommand):
    help = "imports reviewers"

    def add_arguments(self, parser):
        parser.add_argument(
            "filename",
            type=str,
            help="Filename of XLS",
        )

    def assign_username(self, username):
        """
        For cases when name and surname are same but email address is different.
        (For example a reviewer switched email addresses from one year to another.)
        """
        username_assigned = username
        count = 1
        while User.objects.filter(username=username_assigned).exists():
            username_assigned = "%s%d" % (username, count)
            count += 1
        return username_assigned

    def handle(self, *args, **options):
        # User.objects.filter(is_staff=False).delete()

        wb = xlrd.open_workbook(options.get("filename"))
        sheet = wb.sheet_by_index(0)
        max_year = -1

        for row_idx in range(0, sheet.nrows):
            # mandatory items:
            first_name = sheet.cell(row_idx, 0).value.strip()
            last_name = sheet.cell(row_idx, 1).value.strip()
            email = sheet.cell(row_idx, 2).value.strip()

            if len(first_name) <= 0 and len(last_name) <= 0 and len(email) <= 0:
                # ignore empty rows
                continue
            if len(email) <= 0:
                print(
                    "email address empty for %s %s, ignoring" % (first_name, last_name)
                )
                continue

            # optional: year in which given user is expected to do reviews
            year = None
            if sheet.ncols > 3:
                try:
                    year = sheet.cell(row_idx, 3).value
                    if isinstance(year, str):
                        year = year.strip()
                    year = int(year)
                    if year > max_year:
                        max_year = year
                except ValueError as e:
                    print(
                        "unable to parse year for %s %s <%s>, ignoring"
                        % (first_name, last_name, email)
                    )

            # optional: board member flag
            is_board_member = False
            if sheet.ncols > 4:
                is_board_member = sheet.cell(row_idx, 4).value.strip().lower() in [
                    "true",
                    "1",
                    "t",
                    "y",
                    "yes",
                ]

            (user, created) = User.objects.get_or_create(
                email=email,
            )
            if created:
                username = self.assign_username(first_name + last_name)
                user.username = username
                user.first_name = first_name
                user.last_name = last_name
                user.is_active = True
                user.save()
                print("user %s created with user name %s" % (email, username))
            else:
                print("user %s already exists, left as is (not changed)" % email)

            (reviewer, created) = Reviewer.objects.get_or_create(
                user=user,
            )
            reviewer.year = year
            reviewer.is_board_member = is_board_member
            reviewer.save()
            if created:
                print(
                    "reviewer entry for user %s (user name %s) created"
                    % (email, user.username)
                )
            else:
                print(
                    "reviewer entry for user %s (user name %s) already exists, adjusted with values from input file"
                    % (email, user.username)
                )

        if max_year > 0:
            # max_year considered as "current year" e.g. "year for which we're going to make reviewers after this import".
            # Hence flag all users who are not staff and whose year != max_year as inactive, thus:
            # 1) they will not be assigned reviews later on
            # 2) they will not be able to login
            users = User.objects.filter(is_staff=False, is_active=True).exclude(
                reviewer__year=max_year
            )
            for user in users:
                user.is_active = False
                user.save()
                print(
                    "user %s (user name %s) not reviewing in %d => marking as inactive"
                    % (user.email, user.username, max_year)
                )
