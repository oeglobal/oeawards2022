import random
from math import ceil

from django.contrib.auth.models import User
from django.core.management import BaseCommand
from django.db.models import Count

from web.models import Entry, Rating
from web.utils_gform import GForm


class Command(BaseCommand):
    help = "Creates blank ratings and assign them to reviewers"

    def add_arguments(self, parser):
        parser.add_argument(
            "--commit",
            action="store_true",
            dest="commit",
            help="Commit the changes",
        )
        parser.add_argument(
            "--reviews",
            action="store",
            dest="reviews",
            help="Number of reviews per entry",
        )

    def handle(self, *args, **options):
        commit = options.get("commit")
        reviews = int(options.get("reviews"))
        self._create_ratings_for_individual_nominations(commit=commit, reviews=reviews)
        self._create_ratings_for_the_rest(commit=commit, reviews=reviews)

    def _create_ratings_for_individual_nominations(self, commit=False, reviews=3):
        self._create_ratings(True, commit, reviews)

    def _create_ratings_for_the_rest(self, commit=False, reviews=3):
        self._create_ratings(False, commit, reviews)

    def _get_min_rating_per_usr_count(self, users):
        return (
            users.annotate(Count("rating"))
            .order_by("rating__count")
            .first()
            .rating__count
        )

    def _get_users_with_min_rating_count(self, users):
        min_count = self._get_min_rating_per_usr_count(users)
        return list(users.annotate(Count("rating")).filter(rating__count=min_count))

    def _create_ratings(self, for_individual_nominations, commit=False, reviews=3):
        if for_individual_nominations:
            title = "Individual nominations"
        else:
            title = "Other nominations"

        if not commit:
            self.stdout.write("===== %s: DRY RUN =====" % title)
        else:
            self.stdout.write("===== %s: Commiting =====" % title)

        if for_individual_nominations:
            users = User.objects.filter(reviewer__is_board_member=True)
            entries = Entry.objects.filter(
                category__name=GForm.CATEGORY_INDIVIDUAL_AWARDS
            )
        else:
            users = User.objects
            entries = Entry.objects.exclude(
                category__name=GForm.CATEGORY_INDIVIDUAL_AWARDS
            )
        users = users.filter(is_staff=False, is_active=True)

        self.stdout.write(
            "There are {} reviewers in the system and {} entries".format(
                users.count(), entries.count()
            )
        )
        if users.count() <= 0:
            self.stdout.write("No users, aborting rating initialization")
            return

        ballots_per_reviewer = entries.count() * reviews / users.count()
        self.stdout.write(
            "With {} reviews per entry this means ~{} entries for each reviewer.".format(
                reviews, ballots_per_reviewer
            )
        )

        if commit:
            if for_individual_nominations:
                Rating.objects.filter(
                    entry__category__name=GForm.CATEGORY_INDIVIDUAL_AWARDS
                ).delete()
            else:
                Rating.objects.exclude(
                    entry__category__name=GForm.CATEGORY_INDIVIDUAL_AWARDS
                ).delete()

            for entry in entries:
                assigned_ballots = 0
                reviewers = self._get_users_with_min_rating_count(users)
                assigned_reviewers = []
                while assigned_ballots < reviews:
                    random_user = random.choice(reviewers)

                    rating = Rating.objects.create(
                        user=random_user, entry=entry, status="empty"
                    )
                    self.stdout.write("{}".format(rating))

                    assigned_ballots += 1
                    assigned_reviewers.append(random_user)
                    reviewers.remove(random_user)
                    if len(reviewers) <= 0:
                        reviewers = self._get_users_with_min_rating_count(users)
                        for reviewer in assigned_reviewers:
                            reviewers.remove(reviewer)

                self.stdout.write(
                    "Entry #{} has {} reviews assigned".format(
                        entry.entry_id, entry.rating_set.count()
                    )
                )
