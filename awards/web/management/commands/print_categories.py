from django.core.management.base import BaseCommand
from web.models import Entry


class Command(BaseCommand):
    help = "Extract and print all possible values of Main Category and their Subcategories from Entry data"

    def handle(self, *args, **kwargs):
        categories = {}  # Dictionary to hold categories and their subcategories

        for entry in Entry.objects.all():
            if entry.data:
                data_dict = entry.data
                main_category = data_dict.get("Main Category")
                subcategory = entry.subcategory
                if main_category:
                    if main_category not in categories:
                        categories[main_category] = set()
                    if subcategory:
                        categories[main_category].add(subcategory)

        print("*** All possible values of Main Categories and their Subcategories: ***")
        print()

        for main_category, subcategories in categories.items():
            print(f"Main Category: {main_category}")
            for subcategory in subcategories:
                print(f"  Subcategory: {subcategory}")
