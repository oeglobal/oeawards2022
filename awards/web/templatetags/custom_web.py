from django import template
from django.template.defaulttags import register
from django.template.defaultfilters import stringfilter


@register.filter
def get_item(dictionary, key):
    return dictionary.get(key)


@register.filter(is_safe=True)
@stringfilter
def a_target_blank(text):
    return text.replace("<a ", '<a target="_blank" ')
