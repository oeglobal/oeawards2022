To login to the OE Awards Review system, please click on the following link:

https://review.awards.oeglobal.org{{ url }}

It will stay active for {{ key_max_age }} days, afterwards you'll have to request a new one.

If you have any questions or problems, please contact OEG staff at awards@oeglobal.org.
