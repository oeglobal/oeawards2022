class GForm:
    INDEX_NOMINATOR_NAME = "1"
    INDEX_NOMINATOR_EMAIL = "2"
    INDEX_NOMINATOR_INSTITUTION = "65"

    INDEX_NOMINEE_NAME = "5"
    INDEX_NOMINEE_EMAIL = "6"
    INDEX_NOMINEE_INSTITUTION = "23"
    INDEX_NOMINEE_LEAD_CONTACT = "67"

    INDEX_SUBCATEGORIES = [
        "49",  # How We Share (Open Practices Awards)
        "52",  # People in the Open (Individual Awards)
        "66",  # What We Share (Assets Awards)
        "68",  # Special Awards
    ]

    KEY_NOMINATOR_FIRST = "First"
    KEY_NOMINATOR_LAST = "Last"
    KEY_NOMINATOR_INSTITUTION = "Institution / Affiliation"
    KEY_NOMINATOR_FIRST_ADJUSTED = "N_" + KEY_NOMINATOR_FIRST
    KEY_NOMINATOR_LAST_ADJUSTED = "N_" + KEY_NOMINATOR_LAST
    KEY_NOMINATOR_INSTITUTION_ADJUSTED = "N_" + KEY_NOMINATOR_INSTITUTION

    KEY_NOMINEE_ABOUT = "About the Nominee"
    KEY_NOMINEE_FIRST = "First"
    KEY_NOMINEE_LAST = "Last"
    KEY_NOMINEE_INSTITUTION = "Institution / Affiliation"
    KEY_NOMINEE_FIRST_ADJUSTED = "C_" + KEY_NOMINEE_FIRST
    KEY_NOMINEE_LAST_ADJUSTED = "C_" + KEY_NOMINEE_LAST
    KEY_NOMINEE_INSTITUTION_ADJUSTED = "C_" + KEY_NOMINEE_INSTITUTION

    KEY_MAIN_CATEGORY = "Main Category"
    KEY_SUBCATEGORY = "Subcategory"
    KEY_TITLE = "Name or Title"

    KEY_ADDITIONAL_SUPPORT_MATERIAL = "Additional Support Material"
    KEY_ADDITIONAL_SUPPORT_MATERIAL_OPTIONAL = "Additional Support Materials or Notes"
    KEY_LETTER_OF_SUPPORT = (
        "Letter of Support (strongly encouraged, required for student award))"
    )
    KEY_NARRATIVE = "Narrative"
    KEY_NOMINATION_MOTIVATION = "Motivation for Nominating"

    KEY_FACEBOOK = "Facebook URL"
    KEY_LINK = "Web Address"
    KEY_LINKEDIN = "LinkedIn URL"
    KEY_MASTODON = "Mastodon"
    KEY_TWITTER = "Twitter"
    KEY_YOUTUBE = "Video Testimonial (optional, but encouraged)"

    CATEGORY_INDIVIDUAL_AWARDS = "Individual Awards"
