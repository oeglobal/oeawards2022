from django.contrib import admin

from .models import Entry, Category, LoginKey, Rating, Reviewer


@admin.register(Entry)
class EntryAdmin(admin.ModelAdmin):
    list_display = ["__str__", "get_entry_link"]
    list_filter = ["category"]


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    pass


@admin.register(Rating)
class RatingAdmin(admin.ModelAdmin):
    list_display = ["__str__", "updated", "status", "user"]
    list_filter = ["status", "user"]


# for newer Django:
# @admin.display(description="Name")
def user_name(obj):
    return "%s %s (%s)" % (obj.user.first_name, obj.user.last_name, obj.user.username)


# for older Django:
user_name.short_description = "Name"


@admin.register(Reviewer)
class ReviewerAdmin(admin.ModelAdmin):
    list_display = [user_name, "is_board_member"]
    list_filter = ["is_board_member"]


admin.site.register(LoginKey)
